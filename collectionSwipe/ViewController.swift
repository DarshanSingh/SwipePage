//
//  ViewController.swift
//  collectionSwipe
//
//  Created by AlienBrainz_mac4 on 12/01/19.
//  Copyright © 2019 Allien Brainz Software Pvt. Ltd. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource {

    var arr = ["page 1","page 2","page 3","page 4","page 5","page 6"]
    @IBOutlet weak var lblPage: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.isPagingEnabled = true
        // Do any additional setup after loading the view, typically from a nib.
        // collection view scroll direction as horizontal
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "page", for: indexPath) as! CollectionViewCell
        cell.lblpage.text = arr[indexPath.row]
        return cell
    }
    
}

